import { useMemo } from 'react';
import PillButton from '../PillButton';

const ShortcodesButtons = ({ shortcodes }) => {
  const parsedShortcodes = useMemo(() => {
    if (!shortcodes) {
      return [];
    }
    if (!Array.isArray(shortcodes)) {
      return [shortcodes];
    }
    return shortcodes;
  }, [shortcodes]);
  return (
    <div className="flex flex-wrap space-x-2 -mb-2">
      {parsedShortcodes.map((shortcode) => (
        <PillButton color="pink" key={shortcode}>
          {`:${shortcode}:`}
        </PillButton>
      ))}
    </div>
  );
};

export default ShortcodesButtons;
