import getClasses from './getClasses';

const PillButton = ({ color, children, ...rest }) => (
  <button
    type="button"
    className={getClasses(color)}
    {...rest}
  >
    {children}
  </button>
);

export default PillButton;
