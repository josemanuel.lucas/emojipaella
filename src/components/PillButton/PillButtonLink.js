import Link from 'next/link';
import getClasses from './getClasses';

const PillButtonLink = ({
  color, children, href, ...rest
}) => (
  <Link href={href}>
    <a
      className={getClasses(color)}
      {...rest}
    >
      {children}
    </a>
  </Link>
);

export default PillButtonLink;
