import classNames from 'classnames';

const defaultClasses = 'px-2 mb-2 inline-flex text-sm leading-6 font-semibold rounded-full transition-colors';
const colorClasses = {
  purple: 'bg-purple-100 text-purple-600 hover:bg-purple-200 hover:text-purple-700',
  pink: 'bg-pink-100 text-pink-600 hover:bg-pink-200 hover:text-pink-700',
  green: 'bg-green-100 text-green-600 hover:bg-green-200 hover:text-green-700',
};

const getClasses = (color) => classNames([defaultClasses, colorClasses[color]]);

export default getClasses;
