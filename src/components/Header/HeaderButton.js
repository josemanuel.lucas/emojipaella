import classNames from 'classnames';

const defaultClasses = 'bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-white';

const colorClasses = {
  purple: 'text-purple-600 hover:text-purple-400 focus:ring-offset-purple-800',
  gray: 'text-gray-600 hover:text-gray-400 focus:ring-offset-gray-800',
};

const HeaderButton = ({ children, color, ariaLabel }) => {
  const classes = classNames([defaultClasses, colorClasses[color]]);
  return (
    <button
      type="button"
      className={classes}
      aria-label={ariaLabel}
    >
      {children}
    </button>
  );
};

export default HeaderButton;
