import { AiOutlineMenu, AiOutlineSearch } from 'react-icons/ai';
import Link from 'next/link';
import Logo from './Logo';
import HeaderButton from './HeaderButton';

const Header = () => (
  <header className="relative shadow-lg py-1 px-3 md:px-4 lg:px-5 bg-gray-100">
    <nav className="flex items-center justify-between">
      <HeaderButton color="purple" ariaLabel="Menu">
        <AiOutlineMenu size={20} />
      </HeaderButton>
      <Link href="/">
        <a title="Home">
          <Logo />
        </a>
      </Link>
      <HeaderButton color="gray" ariaLabel="Search">
        <AiOutlineSearch size={26} />
      </HeaderButton>
    </nav>
  </header>
);

export default Header;
