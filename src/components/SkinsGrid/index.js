import classNames from 'classnames';
import styles from './SkinsGrid.module.css';

const SkinsGrid = ({ children }) => (
  <div className={classNames(['grid gap-4', styles.columns])}>
    {children}
  </div>
);

export default SkinsGrid;
