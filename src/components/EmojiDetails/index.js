import { useMemo } from 'react';
import classNames from 'classnames';

const EmojiDetails = ({ options }) => {
  const parsedOptions = useMemo(
    () => options.filter(({ children }) => children).map((val, idx) => ({
      ...val,
      classes: classNames('px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6', idx % 2 === 0 ? 'bg-gray-50' : 'bg-white'),
    })),
    [options],
  );
  return (
    <dl>
      {parsedOptions.map(({ title, children, classes }) => (
        <div
          className={classes}
          key={title}
        >
          <dt className="text-sm font-medium text-gray-500">{title}</dt>
          <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
            {children}
          </dd>
        </div>
      ))}
    </dl>
  );
};

export default EmojiDetails;
