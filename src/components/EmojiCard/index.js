import { useMemo, useRef } from 'react';
import classNames from 'classnames';
import EmojiDetails from '../EmojiDetails';
import PillButton from '../PillButton';
import ShortcodesButtons from '../ShortcodesButtons';
import TagsLinks from '../TagsLinks';

const EmojiCard = ({ data, reduced = false }) => {
  const textAreaRef = useRef();
  const onClick = (e) => {
    textAreaRef.current.value = e.target.value;
    textAreaRef.current.focus();
    textAreaRef.current.select();
    document.execCommand('copy');
    textAreaRef.current.value = null;
  };
  const options = useMemo(
    () => [
      {
        title: 'Hex Code',
        children: data.hexcode ? (
          <PillButton color="purple">{data.hexcode}</PillButton>
        ) : null,
      },
      {
        title: 'Short Codes',
        children: data.shortcodes ? (
          <ShortcodesButtons shortcodes={data.shortcodes} />
        ) : null,
      },
      {
        title: 'Group',
        children: reduced ? null : (
          <span>
            <span className="font-medium text-gray-900">{data.groupTitle}</span>
            {' '}
            <span className="text-sm text-gray-500">
              (
              {data.group}
              )
            </span>
          </span>
        ),
      },
      {
        title: 'Subgroup',
        children: reduced ? null : (
          <>
            <span className="font-medium text-gray-900">
              {data.subgroupTitle}
            </span>
            {' '}
            <span className="text-sm text-gray-500">
              (
              {data.subgroup}
              )
            </span>
          </>
        ),
      },
      {
        title: 'Tags',
        children:
          data.tags && data.tags.length ? <TagsLinks tags={data.tags} /> : null,
      },
      {
        title: 'Type',
        children: data.typeTitle,
      },
      {
        title: 'Version',
        children: data.version,
      },
      {
        title: 'Text',
        children: data.text,
      },
    ],
    [
      data.group,
      data.groupTitle,
      data.hexcode,
      data.shortcodes,
      data.subgroup,
      data.subgroupTitle,
      data.tags,
      data.text,
      data.typeTitle,
      data.version,
      reduced,
    ],
  );
  const buttonClasses = classNames([
    'mr-4',
    reduced ? 'text-6xl' : 'text-9xl',
  ]);
  const headingClasses = classNames([
    'font-bold',
    reduced ? 'text-lg leading-4 text-gray-600' : 'text-2xl leading-6 text-gray-900',
  ]);
  return (
    <div className="bg-white shadow overflow-hidden sm:rounded-lg">
      <div className="px-4 py-5 sm:px-6 flex items-center">
        <textarea
          ref={textAreaRef}
          readOnly
          className="fixed w-2 h-2 bg-transparent border-0 appearance-none resize-none transform -translate-x-full -translate-y-full top-0 outline-none"
        />
        <button
          value={data.emoji}
          onClick={onClick}
          type="button"
          className={buttonClasses}
        >
          {data.emoji}
        </button>
        <h1 className={headingClasses}>
          {data.annotation}
        </h1>
      </div>
      <div className="border-t border-gray-200">
        <EmojiDetails options={options} />
      </div>
    </div>
  );
};

export default EmojiCard;
