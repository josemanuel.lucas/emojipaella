import classNames from 'classnames';
import slugify from 'slugify';
import styles from './EmojiList.module.css';

const COMMON_LINK_CLASSES = [
  'flex',
  'align-center',
  'justify-center',
  'rounded-md',
  'border',
  'border-gray-200',
  'shadow-sm',
  'hover:shadow-md',
];

const SIZE = 'md';

const EmojiList = ({ data }) => {
  const gridClass = classNames('grid', styles[`grid-${SIZE}`]);
  const linkClass = classNames([
    ...COMMON_LINK_CLASSES,
    styles.link,
    styles[`link-${SIZE}`],
  ]);
  return (
    <div className={gridClass}>
      {data.map((v) => (
        <a className={linkClass} href={`/emoji/${slugify(v.annotation)}`} key={v.order}>
          {v.emoji}
        </a>
      ))}
    </div>
  );
};

export default EmojiList;
