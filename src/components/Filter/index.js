import Link from 'next/link';
import { useRouter } from 'next/router';
import classNames from 'classnames';

const FilterLink = ({
  isActive, href, children, extraClasses = '',
}) => {
  const activeClasses = isActive
    ? 'bg-purple-700 text-purple-100'
    : 'bg-purple-100 text-purple-800 hover:bg-gray-100 hover:text-purple-900';
  const classes = classNames([
    'text-xs md:text-sm uppercase px-2 py-1.5 transition-colors flex-1 text-center font-medium',
    extraClasses,
    activeClasses,
  ]);
  return (
    <Link href={href}>
      <a className={classes}>{children}</a>
    </Link>
  );
};

const Filter = () => {
  const { asPath } = useRouter();
  return (
    <div className="max-w-md mx-auto">
      <nav role="group" className="flex w-full shadow-md rounded-xl">
        <FilterLink
          isActive={asPath === '/'}
          href="/"
          extraClasses="rounded-l-xl border-r-0"
        >
          Simple
        </FilterLink>
        <FilterLink
          isActive={asPath === '/categorized'}
          href="/categorized"
          extraClasses="border-l-0 border-r-0"
        >
          Categorized
        </FilterLink>
        <FilterLink
          isActive={asPath === '/hierarchized'}
          extraClasses="rounded-r-xl border-l-0"
          href="/hierarchized"
        >
          Hierarchized
        </FilterLink>
      </nav>
    </div>
  );
};

export default Filter;
