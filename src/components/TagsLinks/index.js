import PillButtonLink from '../PillButton/PillButtonLink';

const TagsLinks = ({ tags }) => (
  <div className="flex flex-wrap space-x-2 -mb-2">
    {tags.map((tag) => (
      <PillButtonLink color="green" key={tag} href={`/tag/${tag}`}>
        {tag}
      </PillButtonLink>
    ))}
  </div>
);

export default TagsLinks;
