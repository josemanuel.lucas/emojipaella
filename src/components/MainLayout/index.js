import Header from '../Header';

const MainLayout = ({ children }) => (
  <div className="grid grid-rows-layout h-screen bg-gray-50 subpixel-antialiased">
    <Header />
    <main className="overflow-y-auto px-3 md:px-4 lg:px-5">
      {children}
    </main>
  </div>
);

export default MainLayout;
