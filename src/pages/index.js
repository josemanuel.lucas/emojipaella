import Head from 'next/head';
import emojiService from '../emojiService';
import MainLayout from '../components/MainLayout';
import Filter from '../components/Filter/index';
import EmojiList from '../components/EmojiList/index';

const Home = ({ data }) => (
  <>
    <Head>
      <title>Home</title>
    </Head>
    <MainLayout>
      <div className="my-7 md:my-8 lg:my-10">
        <Filter />
      </div>
      <div>
        <EmojiList data={data} />
      </div>
    </MainLayout>
  </>
);

export default Home;

export async function getStaticProps() {
  const data = await emojiService.simple();
  if (!data) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      data,
    },
  };
}
