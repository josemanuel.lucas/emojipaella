import Head from 'next/head';
import EmojiList from '../components/EmojiList/index';
import Filter from '../components/Filter/index';
import MainLayout from '../components/MainLayout';
import emojiService from '../emojiService';

const Home = ({ data }) => (
  <>
    <Head>
      <title>Home</title>
    </Head>
    <MainLayout>
      <div className="my-7 md:my-8 lg:my-10">
        <Filter />
      </div>
      <div className="space-y-8">
        {data.map((group) => (
          <div>
            <h2 className="font-bold text-xl md:text-2xl leading-6 text-gray-900 mb-3">
              {group.title}
            </h2>
            <div className="space-y-4">
              {group.items.map((subgroup) => (
                <div>
                  <h3 className="font-medium text-md md:text-lg mb-2 text-gray-700">
                    {subgroup.title}
                  </h3>
                  <EmojiList data={subgroup.items} />
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
    </MainLayout>
  </>
);

export default Home;

export async function getStaticProps() {
  const data = await emojiService.hierarchized();
  if (!data) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      data,
    },
  };
}
