import Head from 'next/head';
import emojiService from '../../emojiService';
import MainLayout from '../../components/MainLayout';
import EmojiList from '../../components/EmojiList';
import EmojiCard from '../../components/EmojiCard/index';
import SkinsGrid from '../../components/SkinsGrid/index';

const Emoji = ({ data }) => (
  <>
    <Head>
      <title>
        {data.emoji}
        {' '}
        {data.annotation}
      </title>
    </Head>
    <MainLayout>
      <div className="my-6 md:my-8 lg:my-10">
        <div className="max-w-xl mx-auto">
          <EmojiCard data={data} />
        </div>
        {data.skins && (
          <div className="my-8 md:my-10 lg:my-12 container mx-auto">
            <h2 className="text-xl font-medium text-gray-400 mb-4">
              Skins
            </h2>
            <SkinsGrid>
              {data.skins.map((skin) => (
                <EmojiCard key={skin.annotation} data={skin} reduced />
              ))}
            </SkinsGrid>
          </div>
        )}
      </div>
      <div className="my-8 md:my-10 lg:my-12 container mx-auto">
        <h2 className="text-xl font-medium text-gray-400 mb-4">
          Related emojis
        </h2>
        <EmojiList data={data.related} />
      </div>
    </MainLayout>
  </>
);

export default Emoji;

export async function getStaticProps(context) {
  const { slug } = context.params;
  const data = await emojiService.getBySlug(slug);
  return {
    props: {
      slug,
      data,
    },
  };
}

export async function getStaticPaths() {
  const slugs = await emojiService.listSlugs();
  const paths = slugs.map((slug) => ({
    params: {
      slug,
    },
  }));
  return {
    paths,
    fallback: false,
  };
}
