import Head from 'next/head';
import emojiService from '../../emojiService';
import MainLayout from '../../components/MainLayout';
import EmojiList from '../../components/EmojiList';
import TagsLinks from '../../components/TagsLinks';

const Tag = ({
  data, tags, slug, related,
}) => (
  <>
    <Head>
      <title>
        {slug}
      </title>
    </Head>
    <MainLayout>
      <div className="max-w-xl mx-auto my-7 md:my-8 lg:my-10">
        <h1 className="font-bold text-xl md:text-2xl leading-6 text-gray-900 mb-3">
          {data.length}
          {' '}
          <span className="font-medium">
            Emojis found with tag:
          </span>
          {' '}
          {slug}
        </h1>
        <EmojiList data={data} />
      </div>
      <div className="container mx-auto my-7 md:my-8 lg:my-10">
        <h2 className="font-medium text-md md:text-lg mb-3 text-gray-700">
          Related tags
        </h2>
        <TagsLinks tags={tags} />
      </div>
      <div className="container mx-auto my-7 md:my-8 lg:my-10">
        <h2 className="font-medium text-md md:text-lg mb-3 text-gray-700">
          Related Emojis
        </h2>
        <EmojiList data={related} />
      </div>
    </MainLayout>
  </>
);

export default Tag;

export async function getStaticProps(context) {
  const { slug } = context.params;
  const [data, tags, related] = await Promise.all([
    emojiService.findByTag(slug),
    emojiService.findRelatedTags(slug),
    emojiService.findRelatedByTag(slug),
  ]);
  return {
    props: {
      slug,
      data,
      tags,
      related,
    },
  };
}

export async function getStaticPaths() {
  const tags = await emojiService.listTags();
  const paths = tags.map((slug) => ({
    params: {
      slug,
    },
  }));
  return {
    paths,
    fallback: false,
  };
}
