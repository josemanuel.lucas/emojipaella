import slugify from 'slugify';
import getRelated from '../utils/getRelated';
import getMultipleRelated from '../utils/getMultipleRelated';
import resolve from './resolve';

const byOrder = (a, b) => a.order - b.order;

const simple = async () => {
  const [data, shortcodes] = await Promise.all([
    resolve('en/data.json'),
    resolve('en/shortcodes/emojibase.json'),
  ]);
  return data
    .filter((v) => v.order !== null && v.order !== undefined)
    .sort(byOrder)
    .map((v) => ({
      ...v,
      slug: slugify(v.annotation),
      shortcodes: shortcodes[v.hexcode] || [],
    }));
};

const hierarchized = async () => {
  const [groupsData, emojisData] = await Promise.all([
    resolve('meta/groups.json'),
    simple(),
  ]);
  if (!groupsData || !emojisData) {
    throw Error('Not able to fetch emojis info');
  }
  const { groups, subgroups, hierarchy } = groupsData;
  const parsedData = Object.entries(hierarchy).map(([idx, val]) => ({
    title: groups[idx],
    items: val.map((pos) => ({
      title: subgroups[pos],
      items: emojisData.filter(
        (emojiData) => emojiData.subgroup === Number(pos),
      ),
    })),
  }));
  return parsedData;
};

const categorized = async () => {
  const [groupsData, emojisData] = await Promise.all([
    resolve('meta/groups.json'),
    simple(),
  ]);
  if (!groupsData || !emojisData) {
    throw Error('Not able to fetch emojis info');
  }
  const { groups, hierarchy } = groupsData;
  const parsedData = Object.entries(hierarchy).map(([idx]) => ({
    title: groups[idx],
    items: emojisData.filter((emojiData) => emojiData.group === Number(idx)),
  }));
  return parsedData;
};

const listSlugs = async () => {
  const emojiList = await simple();
  return emojiList.map(({ slug }) => slug);
};

const reg = /[a-zA-Z0-9_]/gi;

const listTags = async () => {
  const emojiList = await simple();
  const tags = emojiList
    .reduce((acc, curr) => [...acc, ...curr.tags], [])
    .filter((v) => reg.test(v));
  return Array.from(new Set(tags)).sort();
};

const getBySlug = async (slug) => {
  const [emojiList, meta] = await Promise.all([
    simple(),
    resolve('meta/groups.json'),
  ]);
  const found = emojiList.find((v) => v.slug === slug);
  const groupTitle = meta.groups[`${found.group}`];
  const subgroupTitle = meta.subgroups[`${found.subgroup}`];
  const typeTitle = found.type === 0 ? 'text' : 'emoji';
  const related = getRelated(emojiList, found);
  return {
    ...found,
    groupTitle,
    subgroupTitle,
    typeTitle,
    related,
  };
};

const findByTag = async (tag) => {
  const emojiList = await simple();
  return emojiList.filter((v) => v.tags.includes(tag));
};

const findRelatedTags = async (tag) => {
  const emojiList = await simple();
  const tags = emojiList.reduce((acc, curr) => {
    if (!curr.tags.includes(tag)) {
      return acc;
    }
    return [...acc, ...curr.tags];
  }, []);
  return Array.from(new Set(tags))
    .filter((v) => reg.test(v))
    .sort();
};

const findRelatedByTag = async (tag) => {
  const emojiList = await simple();
  const items = await findByTag(tag);
  return getMultipleRelated(emojiList, items);
};

const emojiService = {
  hierarchized,
  categorized,
  simple,
  listSlugs,
  getBySlug,
  findByTag,
  listTags,
  findRelatedTags,
  findRelatedByTag,
};

export default emojiService;
