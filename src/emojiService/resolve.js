const BASE_URL = 'https://cdn.jsdelivr.net/npm/emojibase-data@6.0.0/';

const dataCache = {};
const currentPromises = {};

const fetchResource = (url) => {
  console.log(`FETCHING: ${url}`);
  return fetch(`${BASE_URL}${url}`).then((res) => res.json());
};

const resolve = async (url) => {
  console.log(Object.keys(dataCache).length > 0, Object.keys(currentPromises).length > 0);
  if (dataCache[url]) {
    return dataCache[url];
  }
  if (currentPromises[url]) {
    const data = await currentPromises[url];
    return data;
  }
  currentPromises[url] = fetchResource(url);
  const data = await currentPromises[url];
  dataCache[url] = data;
  return data;
};

export default resolve;
