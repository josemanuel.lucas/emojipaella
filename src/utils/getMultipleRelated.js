import getRelated, { RELATED_RESULTS } from './getRelated';
import unique from './unique';

const getMultipleRelated = (emojiList, items) => {
  const result = items.reduce((acc, curr) => [...acc, ...getRelated(emojiList, curr)], []);
  return unique(result).sort(() => Math.random() - 0.5).slice(0, RELATED_RESULTS);
};

export default getMultipleRelated;
