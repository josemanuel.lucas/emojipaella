const unique = (data) => Array.from(new Set(data));

export default unique;
