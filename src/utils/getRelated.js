import unique from './unique';

export const RELATED_RESULTS = 32;

const getRelated = (emojiList, item) => {
  let related = emojiList
    .sort(() => Math.random() - 0.5)
    .filter((v) => v.tags.find((tag) => item.tags.includes(tag)))
    .slice(0, RELATED_RESULTS);
  if (related.length < RELATED_RESULTS) {
    const subgroupRelated = emojiList.sort(() => Math.random() - 0.5).filter(
      (v) => v.subgroup === item.subgroup,
    );
    related = unique([...related, ...subgroupRelated]).slice(0, RELATED_RESULTS);
  }
  if (related.length < RELATED_RESULTS) {
    const groupRelated = emojiList.sort(() => Math.random() - 0.5).filter(
      (v) => v.group === item.group,
    );
    related = unique([...related, ...groupRelated]).slice(0, RELATED_RESULTS);
  }
  return related;
};

export default getRelated;
