module.exports = {
  purge: ['./src/pages/**/*.js', './src/components/**/*.js'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      gridTemplateRows: {
        layout: 'auto 1fr',
      },
      gridTemplateColumns: {
        emoji: 'repeat(auto-fill, minmax(56px, 1fr))',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
